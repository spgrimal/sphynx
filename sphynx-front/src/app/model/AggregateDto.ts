export class AggregateDto {
  private readonly countryDto: {name:""};
  private readonly aggregateValue: number;

  constructor(countryDto: {name:""}, aggregateValue: number) {
    this.countryDto = countryDto;
    this.aggregateValue = aggregateValue;
  }

}
