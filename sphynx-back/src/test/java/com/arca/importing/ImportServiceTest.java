package com.arca.importing;

import com.arca.aggregation.AggregateService;
import com.arca.aggregation.model.Aggregate;
import com.arca.configuration.CachingConfig;
import com.arca.configuration.properties.ImportingConfig;
import com.arca.configuration.properties.SphynxProperties;
import com.arca.database.RecordRepositoryService;
import com.arca.importing.model.CacheKeyBooleanValue;
import com.arca.importing.model.RecordDto;
import com.arca.messaging.WebsocketMessaging;
import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class ImportServiceTest {

    @Mock
    private SphynxProperties sphynxProperties;
    @Mock
    private DataFileFactory dataFileFactory;
    @Mock
    private RecordRepositoryService recordRepositoryService;
    @Mock
    private WebsocketMessaging websocketMessaging;
    @Mock
    private CacheManager cacheManager;
    @Mock
    private CachingConfig cachingConfig;
    @Mock
    private ImportStatusService importStatusService;
    @Mock
    private AggregateService aggregateService;

    @InjectMocks
    private ImportService importService;

    private final FileSystem fs = Jimfs.newFileSystem(Configuration.unix());

    @Before
    public void beforeTests() {
        when(cachingConfig.cacheManager()).thenReturn(cacheManager);
        ImportingConfig importingConfig = new ImportingConfig();
        importingConfig.setLinesGranularity(1L);
        when(sphynxProperties.getImporting()).thenReturn(importingConfig);
    }

    @Test
    public void initCacheNbLines() throws IOException {
        ImportingDataFile dataFile = createDatafileWithNbLines(4);
        when(dataFileFactory.createFromProperties()).thenReturn(dataFile);
        ConcurrentMap<Object, Object> fileNbLinesHashMap = new ConcurrentHashMap<>();
        Cache cacheFileNbLines = new ConcurrentMapCache(CachingConfig.FILE_LINES_CACHE_NAME,fileNbLinesHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME)).thenReturn(cacheFileNbLines);

        long nbLines = importService.initCacheNbLines();

        Assertions.assertThat(nbLines).isEqualTo(4);
        Long nbLinesInCache = cacheFileNbLines.get(CachingConfig.FILE_LINES_CACHE_KEY,Long.class);
        assertNotNull(nbLinesInCache);
        assertEquals(nbLinesInCache.longValue(),4L);
    }

    @Test
    public void getNbLinesOnExistingFile() throws IOException {
        ImportingDataFile dataFile = createDatafileWithNbLines(2);
        when(dataFileFactory.createFromProperties()).thenReturn(dataFile);
        ConcurrentMap<Object, Object> fileNbLinesHashMap = new ConcurrentHashMap<>();
        Cache cacheFileNbLines = new ConcurrentMapCache(CachingConfig.FILE_LINES_CACHE_NAME,fileNbLinesHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME)).thenReturn(cacheFileNbLines);

        long nbLines = importService.getNbLines(CachingConfig.FILE_LINES_CACHE_KEY);

        Assertions.assertThat(nbLines).isEqualTo(2);
    }

    @Test
    public void getNbLinesOnExistingFileWithCache() {
        ConcurrentMap<Object, Object> fileNbLinesHashMap = new ConcurrentHashMap<>();
        fileNbLinesHashMap.put(CachingConfig.FILE_LINES_CACHE_KEY,2L);
        Cache cacheFileNbLines = new ConcurrentMapCache(CachingConfig.FILE_LINES_CACHE_NAME,fileNbLinesHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME)).thenReturn(cacheFileNbLines);

        long nbLines = importService.getNbLines(CachingConfig.FILE_LINES_CACHE_KEY);

        Assertions.assertThat(nbLines).isEqualTo(2);
    }

    @Test
    public void getNbLinesOnLargeExistingFile() throws IOException {
        ImportingDataFile dataFile = createDatafileWithNbLines(100000);
        when(dataFileFactory.createFromProperties()).thenReturn(dataFile);
        ConcurrentMap<Object, Object> fileNbLinesHashMap = new ConcurrentHashMap<>();
        Cache cacheFileNbLines = new ConcurrentMapCache(CachingConfig.FILE_LINES_CACHE_NAME,fileNbLinesHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME)).thenReturn(cacheFileNbLines);

        long nbLines = importService.getNbLines(CachingConfig.FILE_LINES_CACHE_KEY);

        Assertions.assertThat(nbLines).isEqualTo(100000L);
    }

    @Test(expected = ImportException.class)
    public void getNbLinesOnMissingFilePath() {
        ImportingDataFile dataFile = new ImportingDataFile((Path) null);
        when(dataFileFactory.createFromProperties()).thenReturn(dataFile);
        ConcurrentMap<Object, Object> fileNbLinesHashMap = new ConcurrentHashMap<>();
        Cache cacheFileNbLines = new ConcurrentMapCache(CachingConfig.FILE_LINES_CACHE_NAME,fileNbLinesHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME)).thenReturn(cacheFileNbLines);
        ConcurrentMap<Object, Object> importingHashMap = new ConcurrentHashMap<>();
        Cache cacheImport = new ConcurrentMapCache(CachingConfig.IMPORTING_CACHE_NAME,importingHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME)).thenReturn(cacheImport);

        importService.getNbLines(CachingConfig.FILE_LINES_CACHE_KEY);
    }

    @Test(expected = ImportException.class)
    public void getNbLinesOnMissingFilePathAsString() {
        ImportingDataFile dataFile = new ImportingDataFile((String) null);
        when(dataFileFactory.createFromProperties()).thenReturn(dataFile);
        ConcurrentMap<Object, Object> fileNbLinesHashMap = new ConcurrentHashMap<>();
        Cache cacheFileNbLines = new ConcurrentMapCache(CachingConfig.FILE_LINES_CACHE_NAME,fileNbLinesHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME)).thenReturn(cacheFileNbLines);
        ConcurrentMap<Object, Object> importingHashMap = new ConcurrentHashMap<>();
        Cache cacheImport = new ConcurrentMapCache(CachingConfig.IMPORTING_CACHE_NAME,importingHashMap,false);
        when(cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME)).thenReturn(cacheImport);

        importService.getNbLines(CachingConfig.FILE_LINES_CACHE_KEY);
    }

    @Test
    public void getImportingStateNotInCache() {
      ConcurrentMap<Object, Object> importingStateHashMap = new ConcurrentHashMap<>();
      Cache cacheImport = new ConcurrentMapCache(CachingConfig.IMPORTING_CACHE_NAME,importingStateHashMap,false);
      when(cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME)).thenReturn(cacheImport);

      Boolean importingState = importService.getImportingState(CachingConfig.IMPORTING_CACHE_KEY);

      assertFalse(importingState);
    }

    @Test
    public void getImportingStateInCache() {
      ConcurrentMap<Object, Object> importingStateHashMap = new ConcurrentHashMap<>();
      importingStateHashMap.put(CachingConfig.IMPORTING_CACHE_KEY,Boolean.TRUE);
      Cache cacheImport = new ConcurrentMapCache(CachingConfig.IMPORTING_CACHE_NAME,importingStateHashMap,false);
      when(cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME)).thenReturn(cacheImport);

      Boolean importingState = importService.getImportingState(CachingConfig.IMPORTING_CACHE_KEY);

      assertTrue(importingState);
    }

    @Test
    public void setImportingState() {
      ConcurrentMap<Object, Object> importingStateHashMap = new ConcurrentHashMap<>();
      Cache cacheImport = new ConcurrentMapCache(CachingConfig.IMPORTING_CACHE_NAME,importingStateHashMap,false);
      when(cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME)).thenReturn(cacheImport);
      CacheKeyBooleanValue value = new CacheKeyBooleanValue(CachingConfig.IMPORTING_CACHE_KEY,Boolean.TRUE);

      Boolean importingState = importService.setImportingState(value);

      assertTrue(importingState);
      Boolean ImportStateInCache = cacheImport.get(CachingConfig.IMPORTING_CACHE_KEY,Boolean.class);
      assertNotNull(ImportStateInCache);
      assertTrue(ImportStateInCache);
    }

    @Test
    public void getNbLinesImported() {
      long nbImportedLinesSendByService = 18L;
      when(importStatusService.getImportLineNumber()).thenReturn(nbImportedLinesSendByService);

      long nbImportedLines = importService.getNbLinesImported();

      assertEquals(nbImportedLines,nbImportedLinesSendByService);
      verify(importStatusService, new Times(1)).getImportLineNumber();
    }

    @Test
    public void emptyDatabase() {
      importService.emptyDatabase();

      verify(recordRepositoryService, new Times(1)).emptyDatabase();
      verify(importStatusService, new Times(1)).saveImportLineNumber(eq(0L), eq(false));
      verify(websocketMessaging, new Times(1)).sendProgress(eq(0L),eq(0L));
    }

    @Test
    public void startImportFile() throws Exception {
      long nbLinesInFile = 8L;
      ImportingDataFile dataFile = createDatafileWithNbLines(nbLinesInFile);
      when(dataFileFactory.createFromProperties()).thenReturn(dataFile);
      when(importStatusService.getImportLineNumber()).thenReturn(0L);
      ConcurrentMap<Object, Object> fileNbLinesHashMap = new ConcurrentHashMap<>();
      Cache cacheFileNbLines = new ConcurrentMapCache(CachingConfig.FILE_LINES_CACHE_NAME,fileNbLinesHashMap,false);
      when(cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME)).thenReturn(cacheFileNbLines);
      ConcurrentMap<Object, Object> importingStateHashMap = new ConcurrentHashMap<>();
      importingStateHashMap.put(CachingConfig.IMPORTING_CACHE_KEY,Boolean.TRUE);
      Cache cacheImport = new ConcurrentMapCache(CachingConfig.IMPORTING_CACHE_NAME,importingStateHashMap,false);
      when(cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME)).thenReturn(cacheImport);

      Whitebox.invokeMethod(importService, "startImportFile");

      verify(aggregateService, new Times((int) nbLinesInFile)).add(any(Aggregate.class));
      verify(recordRepositoryService, new Times((int) nbLinesInFile)).save(any(RecordDto.class));
      verify(importStatusService, new Times((int) nbLinesInFile)).saveImportLineNumber(anyLong(),eq(false));
    }

    private ImportingDataFile createDatafileWithNbLines(long nbLines) throws IOException {
        Path foo = fs.getPath("/foo");
        List<String> lines =  new ArrayList<>();
        for (int i = 0; i < nbLines; i++) {
          lines.add("1229212228927,99,Finlande");
        }
        Files.write(foo,lines);
        return new ImportingDataFile(foo);
    }
}
