package com.arca.database.model;

import com.arca.database.RepositoryException;
import com.arca.importing.model.RecordDto;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RecordDaoTest {

  @Test
  public void testFrom() {
    RecordDto record = RecordDto.from("123,4,France");
    RecordDao result = RecordDao.from(record);
    assertThat(result.getCountry().getName()).isEqualTo("France");
    assertThat(result.getTimestamp()).isEqualTo(123);
    assertThat(result.getValue()).isEqualTo(4);
  }

  @Test (expected = RepositoryException.class)
  public void testFromNull() {
    RecordDao result = RecordDao.from(null);
    assertThat(result.getCountry().getName()).isEqualTo("France");
    assertThat(result.getTimestamp()).isEqualTo(123);
    assertThat(result.getValue()).isEqualTo(4);
  }
}
