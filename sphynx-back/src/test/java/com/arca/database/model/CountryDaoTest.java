package com.arca.database.model;

import com.arca.importing.model.CountryDto;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CountryDaoTest {

  @Test
  public void testFrom() throws Exception {
    CountryDto countryDto = new CountryDto("name");
    CountryDao result = CountryDao.from(countryDto);
    assertThat(result).isEqualToComparingFieldByField(countryDto);
  }

  @Test
  public void testFromNull() throws Exception {
    CountryDao result = CountryDao.from(null);
    assertThat(result).isNull();
  }
}
