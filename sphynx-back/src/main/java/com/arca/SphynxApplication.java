package com.arca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class SphynxApplication {
	public static void main(String[] args) {
		SpringApplication.run(SphynxApplication.class, args);
	}
}

