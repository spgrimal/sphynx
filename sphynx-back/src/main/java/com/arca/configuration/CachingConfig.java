package com.arca.configuration;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CachingConfig {

    public final static String FILE_LINES_CACHE_NAME = "nbLines";
    public final static String FILE_LINES_CACHE_KEY = "fileNbLines";
    public final static String IMPORTING_CACHE_NAME = "importing";
    public final static String IMPORTING_CACHE_KEY = "import";

    @Bean
    public CacheManager cacheManager() {
      return new ConcurrentMapCacheManager(FILE_LINES_CACHE_NAME, IMPORTING_CACHE_NAME);
    }
}
