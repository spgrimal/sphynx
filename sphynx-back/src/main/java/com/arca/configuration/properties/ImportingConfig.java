package com.arca.configuration.properties;

public class ImportingConfig {

  private long linesGranularity;

  public long getLinesGranularity() {
    return linesGranularity;
  }

  public void setLinesGranularity(long linesGranularity) {
    this.linesGranularity = linesGranularity;
  }
}
