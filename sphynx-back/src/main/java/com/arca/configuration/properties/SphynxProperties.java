package com.arca.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties ("sphynx")
public class SphynxProperties {

    private DataConfig data;

    private ImportingConfig importing;

    public DataConfig getData() {
        return data;
    }

    public void setData(DataConfig data) {
        this.data = data;
    }

    public ImportingConfig getImporting() {
      return importing;
    }

    public void setImporting(ImportingConfig importing) {
      this.importing = importing;
    }
}
