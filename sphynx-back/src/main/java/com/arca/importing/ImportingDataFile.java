package com.arca.importing;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImportingDataFile {
    private final Path path;

    public ImportingDataFile(Path path) {
        this.path = path;
    }

    public ImportingDataFile(String path) {
        this.path = path != null ? Paths.get(path) : null;
    }

    public boolean exist() {
        Path filePath = getPath();
        return this.path != null && Files.exists(filePath);
    }

    public Path getPath() {
        return path;
    }
}
