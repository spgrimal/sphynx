package com.arca.importing;

import com.arca.configuration.properties.SphynxProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataFileFactory {

    @Autowired
    private SphynxProperties props;

    public ImportingDataFile createFromProperties () {
        return new ImportingDataFile(props.getData().getFilePath());
    }

}
