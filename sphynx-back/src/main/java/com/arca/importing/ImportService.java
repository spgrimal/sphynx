package com.arca.importing;

import com.arca.aggregation.AggregateService;
import com.arca.aggregation.model.Aggregate;
import com.arca.configuration.properties.SphynxProperties;
import com.arca.database.RecordRepositoryService;
import com.arca.importing.model.CacheKeyBooleanValue;
import com.arca.importing.model.RecordDto;
import com.arca.messaging.WebsocketMessaging;
import com.arca.configuration.CachingConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DecimalFormat;

@Service
public class ImportService {

  private final static Logger LOGGER = LoggerFactory.getLogger(ImportService.class);

  private final SphynxProperties sphynxProperties;
  private final DataFileFactory dataFileFactory;
  private final RecordRepositoryService recordRepositoryService;
  private final WebsocketMessaging websocketMessaging;
  private final ImportStatusService importStatusService;
  private final AggregateService aggregateService;
  private final CachingConfig cachingConfig;

  @Autowired
  public ImportService(SphynxProperties sphynxProperties,
                       DataFileFactory dataFileFactory,
                       RecordRepositoryService recordRepositoryService,
                       WebsocketMessaging websocketMessaging,
                       ImportStatusService importStatusService,
                       AggregateService aggregateService,
                       CachingConfig cachingConfig) {
    this.sphynxProperties = sphynxProperties;
    this.dataFileFactory = dataFileFactory;
    this.recordRepositoryService = recordRepositoryService;
    this.websocketMessaging = websocketMessaging;
    this.importStatusService = importStatusService;
    this.aggregateService = aggregateService;
    this.cachingConfig = cachingConfig;
  }

  public long initCacheNbLines() {
    long nbFileLines = getNbLinesInFile();
    Cache cache = cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME);
    cache.put(CachingConfig.FILE_LINES_CACHE_KEY,nbFileLines);
    LOGGER.info("INIT Nb lines in file {}", nbFileLines);
    return nbFileLines;
  }

  @Cacheable(value = CachingConfig.FILE_LINES_CACHE_NAME)
  public long getNbLines(String key) {
    long nbFileLines = 0L;
    Cache cache = cachingConfig.cacheManager().getCache(CachingConfig.FILE_LINES_CACHE_NAME);
    Long cacheValue = cache.get(key, Long.class);
    if (cacheValue != null) {
      nbFileLines = cacheValue;
    }
    return (nbFileLines == 0L) ? getNbLinesInFile() : nbFileLines;
  }

  @Cacheable(value = CachingConfig.IMPORTING_CACHE_NAME)
  public Boolean getImportingState(String key) {
    Boolean importingState = Boolean.FALSE;
    Cache cache = cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME);
    Boolean cacheValue = cache.get(key, Boolean.class);
    if (cacheValue != null) {
      importingState = cacheValue;
    }
    return importingState;
  }

  @CachePut(value = CachingConfig.IMPORTING_CACHE_NAME, key = "#importationKeyValue.key")
  public Boolean setImportingState(CacheKeyBooleanValue importationKeyValue) {
    Cache cache = cachingConfig.cacheManager().getCache(CachingConfig.IMPORTING_CACHE_NAME);
    cache.put(CachingConfig.IMPORTING_CACHE_KEY,importationKeyValue.getValue());
    LOGGER.info("Changing importing state to {}", importationKeyValue.getValue());
    return importationKeyValue.getValue();
  }

  public long getNbLinesImported() {
    return importStatusService.getImportLineNumber();
  }

  public void startImport() {
    Thread newThread = new Thread(this::startImportFile);
    newThread.start();
  }

  public void emptyDatabase() {
    recordRepositoryService.emptyDatabase();
    importStatusService.saveImportLineNumber(0, false);
    websocketMessaging.sendProgress(0, 0);
  }

  private void startImportFile() {
    final ImportingDataFile importingDataFile = getDataFile();

    long lineNumber = importStatusService.getImportLineNumber();
    boolean resumeImport = lineNumber != 0L;
    long totalNbLines = resumeImport ? getNbLines(CachingConfig.FILE_LINES_CACHE_KEY) : initCacheNbLines();

    try (BufferedReader bufr = Files.newBufferedReader(importingDataFile.getPath())) {
      String line;
      long readingNumberLine = 0L;
      while (((line = bufr.readLine()) != null) && getImportingState(CachingConfig.IMPORTING_CACHE_KEY)) {
        if (!resumeImport || readingNumberLine > lineNumber) {
          // It's an initialization or it's an standard import
          RecordDto rec = RecordDto.from(line);
          sendProgress(lineNumber, totalNbLines);
          aggregateService.add(Aggregate.from(rec));
          recordRepositoryService.save(rec);
          importStatusService.saveImportLineNumber(lineNumber,resumeImport);
          lineNumber++;
        }
        readingNumberLine++;
      }
    } catch (IOException e) {
      // Error STOP importing status
      setImportingState(new CacheKeyBooleanValue(CachingConfig.IMPORTING_CACHE_KEY, Boolean.FALSE));
      throw new ImportException("We could not read the datafile contents at the following path: " + importingDataFile.getPath());
    }
    if (lineNumber == totalNbLines) {
      LOGGER.info("Import lines : {}/{} (100%)",lineNumber,totalNbLines);
      LOGGER.info("Importing END : {} lines imported",lineNumber);
      // Eof STOP importing status
      setImportingState(new CacheKeyBooleanValue(CachingConfig.IMPORTING_CACHE_KEY, Boolean.FALSE));
    }
  }

  private long getNbLinesInFile() {
    final ImportingDataFile importingDataFile = dataFileFactory.createFromProperties();
    checkThatDataFileExists(importingDataFile);
    long nbLines = 0L;
    try {
      nbLines = Files.lines(importingDataFile.getPath()).count();
    } catch (IOException ignored) {}
    return nbLines;
  }

  private void sendProgress(long lineNumber, long totalNbLines) {
    if (lineNumber % sphynxProperties.getImporting().getLinesGranularity() == 0) { // each % lines
      final double completion = ((double) lineNumber / totalNbLines) * 100;
      DecimalFormat decimalFormat = new DecimalFormat();
      decimalFormat.setMaximumFractionDigits(4);
      LOGGER.info("Import lines : {}/{} ({}%)",lineNumber,totalNbLines,decimalFormat.format(completion));
      websocketMessaging.sendProgress(totalNbLines,lineNumber);
    }
  }

  private ImportingDataFile getDataFile() {
    ImportingDataFile importingDataFile = dataFileFactory.createFromProperties();
    checkThatDataFileExists(importingDataFile);
    return importingDataFile;
  }

  private void checkThatDataFileExists(ImportingDataFile importingDataFile) {
    if (!importingDataFile.exist()) {
      // Error STOP importing status
      setImportingState(new CacheKeyBooleanValue(CachingConfig.IMPORTING_CACHE_KEY, Boolean.FALSE));
      throw new ImportException("The data file does not exists at the specified path: " + importingDataFile.getPath());
    }
  }

}
