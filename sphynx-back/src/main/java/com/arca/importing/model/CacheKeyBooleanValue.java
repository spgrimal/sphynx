package com.arca.importing.model;

public class CacheKeyBooleanValue {

  private String key;
  private Boolean value;

  public CacheKeyBooleanValue(String key, Boolean value) {
    this.key = key;
    this.value = value;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public Boolean getValue() {
    return value;
  }

  public void setValue(Boolean value) {
    this.value = value;
  }
}
