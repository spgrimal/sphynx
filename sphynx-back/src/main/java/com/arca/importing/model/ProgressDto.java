package com.arca.importing.model;

public class ProgressDto {
    private final long total;
    private final long value;

    public ProgressDto(long total, long value) {
        this.total = total;
        this.value = value;
    }

    public long getTotal() {
        return total;
    }

    public long getValue() {
        return value;
    }
}
