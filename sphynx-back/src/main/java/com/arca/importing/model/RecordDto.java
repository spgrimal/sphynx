package com.arca.importing.model;


import com.arca.importing.ImportException;

import java.util.Arrays;

public class RecordDto {

    private long timestamp;
    private int value;
    private CountryDto countryDto;

    public static RecordDto from(String str) {
        RecordDto recordDto = new RecordDto();
        String[] split = str.split(",");

        checkInputStringIsOk(split);

        recordDto.setTimestamp(Long.valueOf(split[0]));
        recordDto.setValue(Integer.valueOf(split[1]));
        recordDto.setCountry(new CountryDto(split[2]));

        return recordDto;
    }

    private static void checkInputStringIsOk(String[] split) {
        if (split == null || split.length < 3) {
            throw new ImportException("The data to create this record is incorrect: " + Arrays.toString(split));
        }
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public CountryDto getCountry() {
        return countryDto;
    }

    public void setCountry(CountryDto countryDto) {
        this.countryDto = countryDto;
    }
}
