package com.arca.importing;

import com.arca.configuration.CachingConfig;
import com.arca.importing.model.CacheKeyBooleanValue;
import com.arca.messaging.WebsocketMessaging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/import")
public class ImportController {

  private final static Logger LOGGER = LoggerFactory.getLogger(ImportController.class);

  @Autowired
  private ImportService importService;
  @Autowired
  private WebsocketMessaging websocketMessaging;

  @GetMapping("/start")
  public void startImport() {
    LOGGER.info("Importation CALL START");
    if (!importService.getImportingState(CachingConfig.IMPORTING_CACHE_KEY)) {
      LOGGER.info("Starting importation");
      importService.setImportingState(new CacheKeyBooleanValue(CachingConfig.IMPORTING_CACHE_KEY, Boolean.TRUE));
      importService.startImport();
    } else {
      LOGGER.info("Importation is already started");
    }
  }

  @GetMapping("/stop")
  public void stopImport() {
    LOGGER.info("Importation CALL STOP");
    if (importService.getImportingState(CachingConfig.IMPORTING_CACHE_KEY)) {
      LOGGER.info("Stopping importation");
      importService.setImportingState(new CacheKeyBooleanValue(CachingConfig.IMPORTING_CACHE_KEY, Boolean.FALSE));
    } else {
      LOGGER.info("Importation is already stopped");
    }
  }

  @DeleteMapping("/database")
  public Long deleteDatabase() {
    Long nbLinesDone = importService.getNbLinesImported();
    importService.emptyDatabase();
    return nbLinesDone;
  }

  @GetMapping("/lines/total")
  public Long getNbLinesAll() {
    return importService.getNbLines(CachingConfig.FILE_LINES_CACHE_KEY);
  }

  @GetMapping("/lines/done")
  public Long getNbLinesDone() {
    return importService.getNbLinesImported();
  }
}
