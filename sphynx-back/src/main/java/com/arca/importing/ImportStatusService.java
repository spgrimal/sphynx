package com.arca.importing;

import com.arca.database.ImportStatusRepository;
import com.arca.database.model.ImportStatusDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImportStatusService {

  public static final String CURRENT_LINE = "current line";
  private final ImportStatusRepository importStatusRepository;

  @Autowired
  public ImportStatusService(ImportStatusRepository importStatusRepository) {
    this.importStatusRepository = importStatusRepository;
  }

  public long getImportLineNumber() {
    return importStatusRepository.findById(CURRENT_LINE)
      .map(ImportStatusDao::getLineNumber)
      .orElse(0L);
  }

  public void saveImportLineNumber(long lineNumber, boolean isResume) {
    lineNumber = isResume ? lineNumber + 1 : lineNumber; // Line number to store to database
    ImportStatusDao importStatusDao = new ImportStatusDao(CURRENT_LINE, lineNumber);
    importStatusRepository.save(importStatusDao);
  }
}
