package com.arca.database;

import com.arca.database.model.RecordDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecordRepository extends CrudRepository<RecordDao, Long> {

  List<RecordDao> findByTimestampBetween (Long start, Long end);
}
