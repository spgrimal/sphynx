package com.arca.database;

import com.arca.database.model.ImportStatusDao;
import org.springframework.data.repository.CrudRepository;

public interface ImportStatusRepository extends CrudRepository<ImportStatusDao, String> {
}
