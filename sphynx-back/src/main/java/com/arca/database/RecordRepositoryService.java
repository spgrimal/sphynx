package com.arca.database;


import com.arca.database.model.RecordDao;
import com.arca.importing.model.RecordDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecordRepositoryService {

  private RecordRepository recordRepository;

  @Autowired
  public RecordRepositoryService(RecordRepository recordRepository) {
    this.recordRepository = recordRepository;
  }

  public void save(RecordDto rec) {
    RecordDao recordDao = RecordDao.from(rec);
    recordRepository.save(recordDao);
  }

  public void emptyDatabase() {
    recordRepository.deleteAll();
  }
}
