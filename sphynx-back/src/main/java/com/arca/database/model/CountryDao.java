package com.arca.database.model;

import com.arca.importing.model.CountryDto;

import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class CountryDao {

  private String name;

  public static CountryDao from(CountryDto c) {
    if (c == null) {
      return null;
    }

    CountryDao countryDao = new CountryDao();
    countryDao.setName(c.getName());
    return countryDao;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CountryDao countryDao = (CountryDao) o;
    return Objects.equals(name, countryDao.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}
