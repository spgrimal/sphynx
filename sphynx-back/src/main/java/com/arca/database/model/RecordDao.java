package com.arca.database.model;

import com.arca.database.RepositoryException;
import com.arca.importing.model.RecordDto;

import javax.persistence.*;

@Entity (name = "record")
public class RecordDao {

    @Id
    @Column(name = "timestamp")
    private long timestamp;

    @Column(name = "value")
    private int value;

    @Embedded
    @Column(name = "country")
    private CountryDao countryDao;

    public static RecordDao from (RecordDto rec) {
        if (rec == null) {
            throw new RepositoryException("The data to create this record is null.");
        }

        RecordDao recordDao = new RecordDao();
        CountryDao countryDao = CountryDao.from(rec.getCountry());
        recordDao.setCountry(countryDao);
        recordDao.setTimestamp(rec.getTimestamp());
        recordDao.setValue(rec.getValue());

        return recordDao;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public CountryDao getCountry() {
        return countryDao;
    }

    public void setCountry(CountryDao countryDao) {
        this.countryDao = countryDao;
    }
}
