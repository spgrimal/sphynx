package com.arca.aggregation;

import com.arca.aggregation.model.Aggregate;
import com.arca.database.RecordRepository;
import com.arca.database.model.CountryDao;
import com.arca.database.model.RecordDao;
import com.arca.date.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AggregateService {

  private final RecordRepository recordRepository;
  private Map<CountryDao, Long> aggregates = new HashMap<>();

  @Autowired
  public AggregateService(RecordRepository recordRepository) {
    this.recordRepository = recordRepository;
  }

  public void add(Aggregate aggregate) {
    if (!aggregates.containsKey(aggregate.getCountry())) {
      aggregates.put(aggregate.getCountry(), 0L);
    }

    Long oldValue = aggregates.get(aggregate.getCountry());
    aggregates.put(aggregate.getCountry(), oldValue + aggregate.getAggregateValue());
  }

  public List<Aggregate> getAggregates() {
    return aggregates.entrySet()
      .stream()
      .map(entry -> new Aggregate(entry.getKey(), entry.getValue()))
      .collect(Collectors.toList());
  }

  public Map<LocalDate, Long> getTotalByDay(LocalDate startDate, LocalDate endDate) {
    Long startDateMillis = DateConverter.toMillis(startDate);
    Long endDateMillis = DateConverter.toMillis(endDate.plusDays(1));

    return recordRepository.findByTimestampBetween(startDateMillis, endDateMillis)
      .stream()
      .collect(
        Collectors.groupingBy(record -> {
            LocalDate date = DateConverter.toLocaldate(record.getTimestamp());
            return date;
          }
          , Collectors.summingLong(RecordDao::getTimestamp)
        ));
  }
}
