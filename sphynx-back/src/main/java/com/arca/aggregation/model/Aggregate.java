package com.arca.aggregation.model;

import com.arca.database.model.CountryDao;
import com.arca.importing.model.RecordDto;

public class Aggregate {
  private CountryDao countryDao;
  private long aggregateValue;

  public Aggregate(CountryDao countryDao, long aggregateValue) {
    this.countryDao = countryDao;
    this.aggregateValue = aggregateValue;
  }

  public static Aggregate from(RecordDto rec) {
    return new Aggregate(CountryDao.from(rec.getCountry()), rec.getValue());
  }

  public CountryDao getCountry() {
    return countryDao;
  }

  public long getAggregateValue() {
    return aggregateValue;
  }
}
