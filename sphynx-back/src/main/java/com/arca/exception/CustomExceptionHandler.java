package com.arca.exception;

import com.arca.database.RepositoryException;
import com.arca.importing.ImportException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler
{
    @ExceptionHandler(ImportException.class)
    public final ResponseEntity<Object> handleImportExceptions(ImportException ex) {
      CustomResponseError error = new CustomResponseError("Importation error : " + ex.getMessage());
      return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  @ExceptionHandler(RepositoryException.class)
  public final ResponseEntity<Object> handleRepositoryExceptions(RepositoryException ex) {
    CustomResponseError error = new CustomResponseError("Repository error : " + ex.getMessage());
    return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
